<div id="serach-booking-receipt-print" class="receipt_div" style="display: none;  font-family: monaco,Consolas,Lucida Console, monospace;
        ;  width:100%; height:100%">
    <!--Printing Summary-->
    <div width="100%" style="padding:0px;">
        <div>
            <img src="http://www.gamepointindia.com/wp-content/uploads/2017/02/logo.png" style="  display: block;margin-left: auto; margin-right: auto; width: 50%;"
                class="center" alt="gamepointlogo ">

        </div>
    </div>




    <table class="receipt_table" style="text-align: center;
                                font-family: monaco,Consolas,Lucida Console, monospace;
                                font-size: 12px; text-transform: uppercase;width:100%;padding:3px;">
        <tr>
            <td width="100%">
                <b style="padding: 0px;" class="text-capitalize">Netplay Sports Private Limited</b>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <span>
                    Sy.No. 11/33 part, Khanamet Village
                </span>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <span>
                    Serilingampally Mandal, RR District
                </span>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <span>
                    Hyderabad - 500081
                </span>
            </td>
        </tr>
        <td width="100%">
            <span>
                Contact: 040 - 39565353
            </span>
        </td>
        </tr>
        <tr>
            <td width="100%">
                <span class="text-capitalize">

                    GSTIN: 36AAFCN2260H2ZK
                </span>
            </td>
        </tr>

    </table>
    <!-- </div> -->
    <!-- heading end -->

    <table width="100%" class="receipt_table" style="border-top: 1px dashed #000000	;
                        font-family: monaco,Consolas,Lucida Console, monospace;
                        text-align: start; font-size: 12px;padding:3px;  padding-left: 10px;">
        <tr>
            <td width="50%">Receipt NO: </td>
            <td width="50%" id="booking_receipt_Num" style="text-align:start;"></td>
        </tr>
        <tr>
            <td width="50%">Booking ID: </td>
            <td width="50%" id="booking_receipt_bookingID" style="text-align:start;"></td>
        </tr>
      
        <tr>
            <td width="50%">Date:</td>
            <td width="50%" id="booking_receipt_date" style="text-align:start;"></td>
        </tr>
        <tr>
            <td width="50%">Name:</td>
            <td width="50%" style="text-align:start;" id="booking_receipt_Name"></td>
        </tr>
        <tr>
            <td width="50%">Email ID:</td>
            <td width="50%" style="text-align:start;" id="booking_receipt_eamilid"></td>
        </tr>
     
        <tr>
            <td width="50%">Contact No:</td>
            <td width="50%" id="booking_receipt_contact_Num" style="text-align:start;"></td>
        </tr>

       </table>


    <table width="100%" id="booking_receipt" class="receipt_table" style="border-top: 1px dashed #000000;
                         font-family: monaco,Consolas,Lucida Console, monospace;
                         margin-left: 2px;font-size:12px; padding-top:10px  ;padding-right: 15px;padding-left: 10px;">



        <!-- <tr>
            <th style=" border-bottom: 1px dashed #000000	;">
                S.No
            </th>
            <th style=" border-bottom: 1px dashed #000000	;">
                Item
            </th>
            <th style=" border-bottom: 1px dashed #000000	;">
                Price
            </th>
            <th style=" border-bottom: 1px dashed #000000	;">
                Qty
            </th>
            <th style=" border-bottom: 1px dashed #000000	;">
                Total
            </th>
        </tr> -->


        <!-- <tr>

        </tr> -->

        <tr>
            <th  >
                Slot Information
            </th>
        </tr>

    </table>



    <table width="100%" class="booking_receipt_info" style="border-top: 1px dashed #000000;
                                                     font-family: monaco,Consolas,Lucida Console, monospace;
                                                     text-align: start;font-size: 12px;padding-left:  10px;
                                                     padding-right:  15px; margin-top: 10px ;margin-bottom: 10px; ">
        <!-- <tr>
                <td style="text-align:start; width: 22%"> Total Qty:</td>
                <td></td>
                <td></td>
                <td   style="text-align:center;width: 22%"  id="total_qty_in_receipt"></td>
                <td></td>
        </tr>  -->


        <tr>
            <td width="50%">Sport:</td>
            <td width="50%" id="booking_receipt_sport_id" style="text-align:start;"></td>
        </tr>

        <tr>
            <td width="50%">Period:</td>
            <td width="50%" id="booking_receipt_sport_period" style="text-align:start;"></td>
        </tr>

        <tr>
            <td width="50%">Time Slot:</td>
            <td width="50%" id="booking_sport_time" style="text-align:start;"></td>
        </tr>


    <!-- <tr>
            <td style="text-align:start;width: 32%">
             Total Qty:
            </td>
            <td style="text-align:start; width: 2%">
            </td>

            <td style="text-align:center;width: 22%">
            </td>
             <td style="text-align:end;width: 22%">
            </td>
            <td style="text-align:end;width: 22%"  id="">
            </td>

        </tr> -->







    </table>




   <table width="100%" class="receipt_table" style="font-size: 12px; 
                         font-family: monaco,Consolas,Lucida Console, monospace;
                         padding-left:  10px;padding-right: 15px;">

        <tr>
            <td width="50%">Amount Paid:</td>
            <td width="50%" style="text-align:right;" id="booking_receipt_amount_paid"></td>
        </tr>



        <!-- <tr>
            <td width="50%">Discount:</td>
            <td width="50%" style="text-align:right;" id=""></td>
        </tr>


        <tr>
            <td width="50%">Credits:</td>
            <td width="50%" style="text-align:right;" id=""></td>
        </tr>


        <tr>
            <td width="50%" style="font-weight: bold;"> Total: </td>
            <td width="50%" style="text-align:right;font-weight: bold;" id=""></td>
        </tr>
 -->

    </table> 

    <!-- <table width="100%" class="" style="border-top: 1px dashed #000000;
                         font-family: monaco,Consolas,Lucida Console, monospace;
                         text-align: start;font-size: 12px;padding-left:  10px;padding-right:  15px; "> -->

<!--
        <tr>
            <td width="50%">Total Qty:</td>
            <td width="50%" style="text-align:right;" id="total_qty_in_receipt"></td>
        </tr> -->


        <!-- <tr>
            <td width="50%">Taxable Amount:</td>
            <td width="50%" style="text-align:right;" id=""></td>
        </tr>




        <tr>
            <td width="50%">CGST@9%:</td>
            <td width="50%" style="text-align:right;" id=""></td>
        </tr>

        <tr>
            <td width="50%">SGST@9%:</td>
            <td width="50%" style="text-align:right;" id=""></td>
        </tr>



        <tr>
            <td width="50%">Balance:</td>
            <td width="50%" style="text-align:right;" id=""> 0.00</td>
        </tr>

    </table>


    <table width="100%" class="receipt_table"   id="" style="border-bottom: 1px dashed #000000;font-size: 12px;padding:0px;
                                font-family: monaco,Consolas,Lucida Console, monospace;
                                padding-left:  10px;padding-right:  15px;">
 -->


        <!-- <tr>
            <td width="50%">Paid via <span > </span> </td>
            <td width="50%" style="text-align:right;" id="paid_amount_in_receipt"> </td>
        </tr> -->
        <!-- <tr>
                                                        <td width="50%" id="payment_via_in_receipt1">Card</td>
                                                        <td width="50%" style="text-align:right;"></td>
                                                 </tr> -->

    <!-- </table> -->
    <table class="receipt_table" style="text-align: center; font-size: 12px;
                                font-family: monaco,Consolas,Lucida Console, monospace;
                                margin-top: 20px; padding-left: 10px;padding-right: 15px; text-transform: uppercase;width:100% ;padding:3px;">
        <tr>
            <td width="100%">
                <b style="text-transform: uppercase;padding: 0px;" class="text-capitalize">
                    Amount paid is not refundable or transferable
                </b>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <span>
                    ***THANK YOU & VISIT AGAIN***
                </span>
            </td>
        </tr>
    </table>


</div>
