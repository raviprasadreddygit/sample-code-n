var slot_book_Arr  = []


var booking_info_data = []

var booking_credits_info = []

var booking_discount_info = []

var result_price = '';

var add_on_count = []

var taxable_amount = 0;

var total_amount = 0;


var paidViaArray_var = ''
var currentBookings_Data_length = []

var booking_add_on_info = '';

var sum_add_on_count = 0;

var total_amount_in_receipt = []

var slot_booking_addons = '';

var slot_booking_add_on_info = '';

var receipt_headings = '';

var booking_discountType_val = 1;
var booking_discount_val = 0;

var booking_amount_val = '';


var booking_modal_receipt_id = '';


var no_of_qty = 0;

$(document).ready(function () {



    

      //  $("#walkin_price").attr('disabled', true);
              
       // $("#calendarbookingslots #walkin_price").css('background-color', 'black');


    // $(window).scroll(function() {
    //     if ($(this).scrollTop() > 50 ) {
    //         $('.scrolltop:hidden').stop(true, true).fadeIn();
    //     } else {
    //         $('.scrolltop').stop(true, true).fadeOut();
    //     }
    // });
    // $(function(){$(".scroll").click(function(){
    //     $("html,body").animate({scrollTop:$(".thetop").offset().top},"1000");return false})})







    var CURRENT_SELECTED_OPTION = 3;

    var BOOKING_SELECTED_SELECTED_OPTION = 1;
    //Onclick Function container
    window.ppu = {};
    var PRICE = 0;



    $('#booking_discount').val(0);


    $("#selectedbookingtype").change(function () {
        CURRENT_SELECTED_OPTION = $("#selectedbookingtype").val()

        if (CURRENT_SELECTED_OPTION === "3") {

          

          
            $('#walkin_price').val(PRICE);
            $('#booking_amount').val(PRICE)
            $('#booking_discount').val('0');

            $('#hide_context_id').show()

        } else {


            $('#walkin_price').val(0);
            $('#booking_amount').val(0)

            $('#hide_context_id').hide()
        }



    });


    $("#booking_discountType").val(1)

    $('#booking_amount').val(PRICE)
    $('#booking_discount').on("keyup", function () {
        if ($('#booking_discount').val() === "") {
            $('#booking_amount').val(PRICE)
        } else {
            booking_discount_val = $('#booking_discount').val();
            booking_amount_val = parseFloat(parseFloat((PRICE)) - (parseFloat($('#booking_discount').val())));
            $('#booking_amount').val(booking_amount_val)
        }
    });

    $("#booking_discountType").change(function () {

        BOOKING_SELECTED_SELECTED_OPTION = $("#booking_discountType").val()

        $('#booking_amount').val(PRICE)

        if (BOOKING_SELECTED_SELECTED_OPTION === "1") {

            //  booking_discount_val =   $('#booking_discount').val('0');
            $('#booking_discount').on("keyup", function () {
                if ($('#booking_discount').val() === "") {
                    $('#booking_amount').val(PRICE)
                } else {
                    booking_discount_val = $('#booking_discount').val();
                    booking_amount_val = parseFloat(parseFloat((PRICE)) - (parseFloat($('#booking_discount').val())));
                    $('#booking_amount').val(booking_amount_val)
                }
            });


        } else {

            $('#booking_amount').val(PRICE)

        }

    });






    $("#bulkbooking_type").change(function () {
        CURRENT_SELECTED_OPTION = $("#bulkbooking_type").val()
        if (CURRENT_SELECTED_OPTION != 3) {
            $('#walkin_price').val(0);
            $('#booking_amount').val(0)
            $('#booking_discount').val(0);

        } else {
          

            $('#walkin_price').val(PRICE);
            $('#booking_amount').val(PRICE)
            $('#booking_discount').val(0);


        }
    });
    $("#payment_type").change(function () {
        CURRENT_SELECTED_OPTION = $("#payment_type").val()
        if (CURRENT_SELECTED_OPTION == 0) {
            $('#walkin_price').val(0);
            $('#booking_amount').val(0)
            $('#booking_discount').val(0);

        } else if (CURRENT_SELECTED_OPTION == 1) {
          
            $('#walkin_price').val(PRICE);
            $('#booking_amount').val(PRICE)
            $('#booking_discount').val(0);


        } else {
            $('#walkin_price').val(PRICE);
            $('#booking_amount').val(PRICE)
            $('#booking_discount').val(0);

        }
    });




    $(function () {
        // Javascript to enable link to tab
        var hash = document.location.hash;
        if (hash) {
            $('.nav-tabs a[href=' + hash + ']').tab('show');
        }

        // Change hash for page-reload
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        });
    });


    //Format function to fill tables.
    String.prototype.format = function () {
        var args = arguments[0];
        return this.replace(/(\{\d+\})/g, function (a) {
            return args[+(a.substr(1, a.length - 2)) || 0];
        });
    };

    $('#walkin_date').datepicker();


    /*ToDo : To restrict date */
    $('#start_date_bulkbooking').datepicker({
        startDate: new Date()
    });
    $('#end_date_bulkbooking').datepicker({
        startDate: new Date()
    });
    $('#start_date_downloadcsv').datepicker();
    $('#end_date_downloadcsv').datepicker();
    $('#bulkbooking_timings').select2();
    $('#bulkbooking_courts').select2();
    //store user choices
    var timingSelect = [];
    var courtSelect = [];
    var daySelect = {};
    window.ppu.closemodal = function closemodal(modal_id) {
        //  $('#receipt_table_id ').append().html('');

        $('#receipt_table_id ').append().html('');

        $('#receipt_table_id').html('');

        $("#booking_print_receipt_id").attr("disabled", false);


        $('#' + modal_id).hide();
    };
    window.ppu.downloadreceipt = function (id) {

        location.href = document.URLS.receipt(id);
    };
    window.ppu.edit_remarks = function () {
        $('#edit_remarks_input').val($('#booking-modal-remarks').text());

        $('#edit_remarks').hide();
        $('#booking-modal-remarks').hide();

        $('#booking-modal-remarks_input').show();
        $('#edit_remarks_submit').show();
    };
    window.ppu.edit_payment = function (cb) {
        data = {
            'booking_id': $('#booking-modal-booking').text(),
            'is_paid': cb.checked
        };

        $.post(document.URLS.edit_remarks(), JSON.stringify(data), function (data, status) {
            if (data.status == 202) {

                swal("successful", "Updated! ", "success");

                //      alert('Updated!');
            } else {
                $('#edit_remarks_input').val(' ');

                swal("Oops", "Update Failed..!", "error")

            }
        });
    };


    $("#booking_print_receipt_id").attr("disabled", false);


    //  window.ppu.PrintPeceipt    = function(){


    $("#booking_print_receipt_id").on("click", function () {

            //   function PrintPeceipt(){

            //   console.log(booking_modal_receipt_id)


            // $('#receipt_table_id').append().text(' ');
            // $('#payment_via_in_receipt').append().text(' ');
            // $('#payment_via_in_receipt').append().html(' ');
            
            // $('#receipt_table_id').append().html(' ');
            // $('#receipt_table_id').html(' ');
            // $('#receipt_table_id').empty();
            // slot_booking_addons = '';
            // slot_booking_add_on_info = ' ';
            // paidViaArray_var = ' ';
            // currentBookings_Data_length = 0;
            // total_amount_in_receipt = [];
            // add_on_count = []

            // booking_credits_info = [];

            // booking_discount_info = [];
            $.ajax({
                url: SERVER + 'booking-receipt-print/' + booking_modal_receipt_id,
                success: function (result) {
                    currentBookings_Data = result;


                    $('#receipt_table_id').append().text(' ');
                    $('#payment_via_in_receipt').append().text(' ');
                    $('#payment_via_in_receipt').append().html(' ');
                    
                    $('#receipt_table_id').append().html(' ');
                    $('#receipt_table_id').html(' ');
                    $('#receipt_table_id').empty();
                    slot_booking_addons = '';
                    slot_booking_add_on_info = ' ';
                    paidViaArray_var = ' ';
                    currentBookings_Data_length = 0;
                    total_amount_in_receipt = [];
                    add_on_count = []
        
                    booking_credits_info = [];
        
                    booking_discount_info = [];
      
                    slot_book_Arr = []; 

            //        $("#booking_print_receipt_id").attr("disabled", true);
      
                    console.log(currentBookings_Data)

                    var ct_dt = currentBookings_Data.response
                    currentBookings_Data_length = ct_dt.length


                    function groupBy(propertyName, array) {
                        var groupedElements = {};
                        for (var i = 0; i < array.length; i++) {
                            var element = array[i];
                            var value = element[propertyName];
                            var group = groupedElements[value];
                            if (group == undefined) {
                                group = [element];
                                groupedElements[value] = group;
                            } else {
                                group.push(element);
                            }
                        }
                        return groupedElements;
                    }

                    result_price = groupBy("slot_price", ct_dt)
                    //     console.log(Object.keys(result_price) )
                    // for (let val of Object.keys(result_price)) {
                    //     console.log(val)
                    // }
                    for (let value of Object.values(result_price)) {
                        // if(Object.keys(resuresult_pricelt) ==    ){
                       // alert(value.length);
                        //    console.logI(Object.keys(resuresult_pricelt))
                        //   }
                    }
                    //   for (let val of Object.keys(result_price)) {
                    //     console.log(val)
                    // if(uniqueArray[index].slot_price ==  val ){
                    //         for (let value of Object.values(result_price)) {
                    //             // if(Object.keys(resuresult_pricelt) ==    ){
                    //                 no_of_qty = value.length
                    //                  alert(value.length); // John, then 30
                    //      //      }
                    //  //   }
                    // }
                    function removeDuplicates(originalArray, prop) {
                        var newArray = [];
                        var lookupObject = {};
                        for (var i in originalArray) {
                            lookupObject[originalArray[i][prop]] = originalArray[i];
                        }
                        for (i in lookupObject) {
                            newArray.push(lookupObject[i]);
                        }
                        return newArray;
                    }

                    var uniqueArray = removeDuplicates(ct_dt, "slot_price");
                    //    console.log("uniqueArray is: " + JSON.stringify(uniqueArray));
                    var paidViaArray = removeDuplicates(ct_dt, "paid_via");


                 //   var recepit_id =  removeDuplicates(ct_dt, "receipt_id");
                    
                 

                    $.each(paidViaArray, function (index) {
                        paidViaArray_var += ` <tr>
                                            <td width="50%">Paid via <span > </span> </td>
                                            <td width="50%" style="text-align:right;" > ${paidViaArray[index].transaction.paid_via } </td>
                                      </tr>`
                    });
                    $('#payment_via_in_receipt').append(`${paidViaArray_var}`);
                    // $.each(uniqueArray, function(index) {
                    //     slot_booking_add_on_info += `                     
                    //     <tr>
                    //            <td style="text-align:start;width: 12%">
                    //                ${index+1}
                    //            </td>
                    //            <td style="text-align:start; width: 22%">
                    //                <span>   ${uniqueArray[index].slot.court.sport_information.sport.name } -${booking_info_data.booked_type}
                    //                &nbsp  Peak Hour - (1 hour)
                    //                </span> <span></span>
                    //            </td>
                    //            <td style="text-align:center;width: 22%">
                    //                 ${parseFloat(uniqueArray[index].slot_price).toFixed(2)}
                    //            </td>
                    //            <td style="text-align:center;width: 22%">
                    //                     ${no_of_qty}
                    //            </td>
                    //             <td style="text-align:end;width: 22%">
                    //                ${parseFloat(uniqueArray[index].slot_price).toFixed(2)}
                    //            </td>
                    //        </tr>   
                    //          <tr>
                    //          <td> </td>
                    //        <td>
                    //            HSN - 999652
                    //        </td>
                    //    </tr>  `
                    // });



                    // $.each(booking_info_data.addon, function (key, value) {
                    //   //  console.log()
                    //     console.log(value);
                    //     //      $("body").append("<div>" + value.finalProduct.DeviceName + "</div>");
                    //     add_on_count.push(value.count)
                    //     var amount_in_rcpt = ''
                    //     //   ab += `${parseFloat(value.equipment.hourly_price).toFixed(2)}`
                    //     amount_in_rcpt += `${parseFloat(parseInt(value.equipment.hourly_price) * parseInt(value.count)).toFixed(2)}`
                    //     total_amount_in_receipt.push(amount_in_rcpt)
                    //     debugger ;
                    //     slot_booking_addons += `<tr>
                    //                                     <td style="text-align:start;width: 12%">
                    //                                             ${key+1}
                    //                                     </td>
                    //                                     <td style="text-align:start; width: 22%">
                    //                                       <span>  Add on - </spn>  ${value.equipment.name} Lease (1 hour)
                    //                                      </td>
                    //                                     <td style="text-align:center;width: 22%">
                    //                                         ${parseFloat(value.equipment.hourly_price).toFixed(2)}
                    //                                     </td>
                    //                                     <td style="text-align:center;width: 22%">
                    //                                           ${value.count}
                    //                                     </td>
                    //                                     <td style="text-align:end;width: 22%">
                    //                                           ${parseFloat(parseInt(value.equipment.hourly_price) * parseInt(value.count)).toFixed(2)}
                    //                                     </td>
                    //                                 </tr>  
                    //                                    <tr>
                    //                                         <td> </td>
                    //                                         <td>
                    //                                             HSN - 999652
                    //                                         </td>
                    //                                    </tr>`
                    // });




                    $.each(currentBookings_Data.response, function (index) {

                    //    console.log(index);
                        $.each(currentBookings_Data.response[index].addon, function (key, value) {

                            console.log(index);
                 


                     
                            slot_book_Arr.push(value)


                    //        console.log(key);

                    //        console.log(currentBookings_Data.response[index].addon)
                          //  console.log(value);
                            //      $("body").append("<div>" + value.finalProduct.DeviceName + "</div>");
                            add_on_count.push(value.count)
                            var amount_in_rcpt = ''
                            //   ab += `${parseFloat(value.equipment.hourly_price).toFixed(2)}`
                            amount_in_rcpt += `${parseFloat(parseInt(value.equipment.hourly_price) * parseInt(value.count)).toFixed(2)}`
                            total_amount_in_receipt.push(amount_in_rcpt)
                            debugger ;



                            var obj_arr_appended = currentBookings_Data.response[index].addon.map(function(currentValue, Index) {
                                currentValue.id = Index
                                return currentValue
                             })


                             debugger ;

                            // console.log(obj_arr_appended)

                        //     slot_booking_addons += `<tr>
                        //                                     <td style="text-align:start;width: 12%">
                        //                                             ${key+1}
                        //                                     </td>
                        //                                     <td style="text-align:start; width: 22%">
                        //                                       <span>  Add on - </spn>  ${value.equipment.name} Lease (1 hour)
                        //                                      </td>
                        //                                     <td style="text-align:center;width: 22%">
                        //                                         ${parseFloat(value.equipment.hourly_price).toFixed(2)}
                        //                                     </td>
                        //                                     <td style="text-align:center;width: 22%">
                        //                                           ${value.count}
                        //                                     </td>
                        //                                     <td style="text-align:end;width: 22%">
                        //                                           ${parseFloat(parseInt(value.equipment.hourly_price) * parseInt(value.count)).toFixed(2)}
                        //                                     </td>
                        //                                 </tr>  
                        //                                    <tr>
                        //                                         <td> </td>
                        //                                         <td>
                        //                                             HSN - 999652
                        //                                         </td>
                        //                                    </tr>`
                         });


                      //  console.log(x)
                        debugger ;

                        receipt_headings = `  <tr>
                                        <th style=" border-bottom: 1px dashed #000000	;">
                                            S.No
                                        </th>              
                                        <th style=" border-bottom: 1px dashed #000000	;">
                                            Item
                                        </th>
                                        <th style=" border-bottom: 1px dashed #000000	;">
                                            Price
                                        </th>
                                        <th style=" border-bottom: 1px dashed #000000	;">
                                            Qty
                                        </th>
                                        <th style=" border-bottom: 1px dashed #000000	;">
                                            Total
                                        </th>
                                    </tr>`;

                        var booking_addondata = [];
                        booking_addondata = booking_info_data.addon
                        //     var booking_add_on_info = '';
                        //            total_amount_in_receipt.push(currentBookings_Data.response[index].slot_price)

                        var total_amt = ''
                        total_amt += `${parseFloat(currentBookings_Data.response[index].slot_price).toFixed(2)}`
                        total_amount_in_receipt.push(total_amt)

                  
                        var book_credit_info = ''
                        book_credit_info += `${currentBookings_Data.response[index].booking_credits}`
                        try {
                             if(book_credit_info == "null"){
                                booking_credits_info.push(0)
                             }
                             else{
                                booking_credits_info.push(book_credit_info)
                             }
                        } catch (error) {
                        booking_credits_info.push(book_credit_info)
                        }
                     
              

                        var book_discount_info = ''
                        book_discount_info += `${currentBookings_Data.response[index].booking_discount}`
                        try {
                            if(book_discount_info == "null"){
                                booking_discount_info.push(0)
                            }
                            else{
                                booking_discount_info.push(book_discount_info)
                            }
                        } catch (error) {
                                    booking_discount_info.push(book_discount_info)

                        }

                    //    booking_discount_info.push(book_discount_info)

                
      
    //                  var c =         moment.utc(currentBookings_Data.response[index].slot.slot_from).format("MMMM D, YYYY HH:mm"); 
                      

                        slot_booking_add_on_info += `                     
                                                        <tr>
                                                            <td style="text-align:start;width: 12%">
                                                                ${index+1}
                                                            </td>

                                                            <td style="text-align:start; width: 22%">

                                                                <span>   ${currentBookings_Data.response[index].slot.court.sport_information.sport.name } -${booking_info_data.booked_type}
                                                                &nbsp  Peak Hour - (${moment.utc(currentBookings_Data.response[index].slot.slot_from).format("MMMM D, YYYY HH:mm") } -  ${moment.utc(currentBookings_Data.response[index].slot.slot_to).format("MMMM D, YYYY HH:mm")})-(${currentBookings_Data.response[index].slot.court.space_name[0].name}) 
                                                                </span> <span></span>
                                                            </td>
                                                            <td style="text-align:center;width: 22%">

                                                                    ${parseFloat(currentBookings_Data.response[index].slot_price).toFixed(2)}
                                                                
                                                            </td>
                                                            <td style="text-align:center;width: 22%">
                                                                    1      
                                                            </td>
                                                                <td style="text-align:end;width: 22%">
                                                                ${parseFloat(currentBookings_Data.response[index].slot_price).toFixed(2)}
                                                            </td>
                                                        </tr>   
                                                            <tr>
                                                            <td> </td>

                                                        <td>
                                                            HSN - 999652
                                                        </td>
                                                    </tr>  `
                    })



                    $.each(slot_book_Arr, function (key, value) {
                                                slot_booking_addons += `<tr>
                                                            <td style="text-align:start;width: 12%">
                                                                    ${key+1}
                                                            </td>
                                                            <td style="text-align:start; width: 22%">
                                                            <span>  Add on - </spn>  ${value.equipment.name} Lease (1 hour)
                                                            </td>
                                                            <td style="text-align:center;width: 22%">
                                                                ${parseFloat(value.equipment.hourly_price).toFixed(2)}
                                                            </td>
                                                            <td style="text-align:center;width: 22%">
                                                                ${value.count}
                                                            </td>
                                                            <td style="text-align:end;width: 22%">
                                                                ${parseFloat(parseInt(value.equipment.hourly_price) * parseInt(value.count)).toFixed(2)}
                                                            </td>
                                                        </tr>  
                                                        <tr>
                                                                <td> </td>
                                                                <td>
                                                                    HSN - 999652
                                                                </td>
                                                        </tr>`

                                                        })



                    sum_add_on_count = add_on_count.reduce((a, b) => a + b, 0);
                    //  var total_price_receipt = total_amount_in_receipt.reduce((a, b) => a + b, 0);

                    var total_price_receipt = total_amount_in_receipt.reduce(function (prev, curr) {
                        return (Number(prev) || 0) + (Number(curr) || 0);
                    });

                    

                    var All_booking_credits = booking_credits_info.reduce(function (prev, curr) {
                        return (Number(prev) || 0) + (Number(curr) || 0);
                    });

          
                    var All_booking_discount = booking_discount_info.reduce(function (prev, curr) {
                        return (Number(prev) || 0) + (Number(curr) || 0);
                    });

       
                    total_amount = parseFloat(total_price_receipt - All_booking_credits - All_booking_discount).toFixed(2)

                    console.log(total_amount);
                    $('#booking_invoice_Discount').text(parseFloat(All_booking_discount).toFixed(2))


                    $('#booking_invoice_Credits').text(parseFloat(All_booking_credits).toFixed(2))

                    //  $('#paid_amount_in_booking_receipt').text(parseFloat(total_amount).toFixed(2))

                    $("#booking_invoice_Amount").text(parseFloat(total_price_receipt).toFixed(2))

                    $("#total_qty_in_receipt").text(sum_add_on_count + currentBookings_Data_length)

                    $("#booking_invoice_Name").text(booking_info_data.name)

                    $("#booking_invoice_Mob_Num").text(booking_info_data.mobile)

                    $("#booking_invoice_Receipt_Id").text(booking_info_data.receipt)


                    $("#booking_invoice_date").text(booking_info_data.bookedat)

                    taxable_amount = (total_amount * 100) / 118;
                    var tax = (total_amount - taxable_amount) / 2;
                    $('#cgst_in_booking_receipt').html(tax.toFixed(2));
                    $('#sgst_in_booking_receipt').html(tax.toFixed(2));
                    $('#Sub_total_in_booking_receipt').html(taxable_amount.toFixed(2));
                    var paid_amount = parseInt((taxable_amount + tax + tax).toFixed(2))


                    // $('#cgst_in_booking_receipt').text(tax.toFixed(2));
                    // $('#sgst_in_booking_receipt').text(tax.toFixed(2));
                    // $('#Sub_total_in_booking_receipt').text(taxable_amount.toFixed(2));
                    // var paid_amount = parseInt((taxable_amount + tax + tax).toFixed(2))




                    $('#paid_amount_in_booking_receipt').text(parseFloat(paid_amount).toFixed(2))

                    //  $('#balance_in_receipt').html(paid_amount.toFixed(2));

                    $('#receipt_table_id').append(`${receipt_headings}  ${slot_booking_add_on_info}  ${slot_booking_addons}`);

                    var contents = document.getElementById("booking-receipt-print").innerHTML;
                    var frame1 = document.createElement('iframe');
                    frame1.name = "frame1";
                    frame1.style.position = "absolute";
                    frame1.style.top = "-1000000px";
                    document.body.appendChild(frame1);
                    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
                    frameDoc.document.open();
                    frameDoc.document.write('<html><head>');
                    frameDoc.document.write('</head><body>');
                    frameDoc.document.write(contents);
                    frameDoc.document.write('</body></html>');
                    frameDoc.document.close();
                    setTimeout(function () {
                        window.frames["frame1"].focus();
                        window.frames["frame1"].print();
                        document.body.removeChild(frame1);
                    }, 500);
                    return false;


                },


                error: function (e) {

                    $("#booking_print_receipt_id").attr("disabled", false);


                    swal("Oops", "something went wrong please try again..!", "error")

                }



            });

        }
        //  window.ppu.PrintPeceipt    = function(){

        // function PrintPeceipt(){
        // var contents = document.getElementById("booking-receipt-print").innerHTML;
        // var frame1 = document.createElement('iframe');
        // frame1.name = "frame1";
        // frame1.style.position = "absolute";
        // frame1.style.top = "-1000000px";
        // document.body.appendChild(frame1);
        // var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        // frameDoc.document.open();
        // frameDoc.document.write('<html><head>');
        // frameDoc.document.write('</head><body>');
        // frameDoc.document.write(contents);
        // frameDoc.document.write('</body></html>');
        // frameDoc.document.close();

        // if(frameDoc.document.close()){


        //     }
        //     setTimeout(function () {
        //         window.frames["frame1"].focus();
        //         window.frames["frame1"].print();
        //         document.body.removeChild(frame1);
        //     }, 500);
        //     return false;
        // })

    )
    $('#booking-receipt-print').hide();

    // end print receipt

    window.ppu.submit_remarks = function () {
        data = {
            'booking_id': $('#booking-modal-booking').text(),
            'remarks': $('#edit_remarks_input').val()
        };
        $.post(document.URLS.edit_remarks(), JSON.stringify(data), function (data, status) {
            if (data.status == 202) {
                $('#booking-modal-remarks').text($('#edit_remarks_input').val());
            } else {
                $('#edit_remarks_input').val(' ');
                swal("Oops", "Update Failed..!", "error")
            }
            $('#edit_remarks').show();
            $('#booking-modal-remarks').show();
            $('#booking-modal-remarks_input').hide();
            $('#edit_remarks_submit').hide();
            //location.href=window.location.href
        });
    };
    var cancelbooking = (function () {
        var booking_ids = {};
        var message = "Cancellation Remarks:";
        return {
            cancel_booking: function () {
                var cancel_confirm = prompt(message);
                if (cancel_confirm) {
                    data = {
                        keys: Object.keys(booking_ids),
                        message: cancel_confirm,
                        is_bulk: "False"
                    };
                    $.post(document.URLS.bookinginfo(), JSON.stringify(data), function (data, status) {

                        //    console.log(data)
                        window.location.replace(document.URLS.ppu());
                    });
                }
            },
            add_cancelbooking: function (id, id_bulk) {
                if (id_bulk != null) {
                    if (confirm("Want to delete whole bulk booking or not ?")) {
                        var arr = {};
                        arr[id_bulk] = true;
                        cancel_confirm = prompt(message);
                        if (cancel_confirm) {
                            data = {
                                keys: Object.keys(arr),
                                message: cancel_confirm,
                                is_bulk: "True"
                            };
                            $.post(document.URLS.bookinginfo(), JSON.stringify(data), function (data, status) {


                                window.location.replace(document.URLS.ppu());
                            });
                            return;
                        }
                    }
                }
                if (!booking_ids[id]) {
                    booking_ids[id] = true;
                    $("#cancelbooking_booking_" + id).css("color", "red");
                    if (Object.keys(booking_ids).length > 0) {
                        $('#cancelbooking_submit').show();
                    }
                } else if (booking_ids[id]) {
                    $("#cancelbooking_booking_" + id).css("color", "#1f1f1f");
                    delete booking_ids[id];
                    if (Object.keys(booking_ids).length < 1) {
                        $('#cancelbooking_submit').hide();
                    }
                }
            }
        };

    })();


    window.ppu.add_cancelbooking = cancelbooking.add_cancelbooking;
    window.ppu.cancel_bookings = cancelbooking.cancel_booking;
    window.ppu.calendar_slot_booking_details = function (id) {

        $('#booked-modal-add-ons').append().html('');


        $('#edit_remarks').show();
        $('#booking-modal-remarks').show();
        $('#booking-modal-remarks_input').hide();
        $('#edit_remarks_submit').hide();
        $.get(document.URLS.bookinginfo(id), function (data, status) {

            //       console.log("booking info");
            //      console.log(data);

            booking_info_data = data;


              console.log(booking_info_data)
            //  console.log(booking_info_data.sport);


            var addondata = [];
            addondata = data.addon

            console.log(addondata)
            var game_price = 0.0;
            var add_on_info = '';
            $('#booked-modal-add-ons').append().html('');
            $.each(data.addon, function (i, item) {
                game_price += addondata[i].price;
                add_on_info += '<span>' + addondata[i].equipment.name + '-' + addondata[i].count + '</span> ';
            });

            booking_modal_receipt_id = data.receipt;

            //  console.log(booking_modal_receipt_id)


            $('#booked-modal-add-ons').append(game_price + "( " + add_on_info + ")");


            $('#booking-modal-date').text(data.slot_date);
            $('#booking-modal-time').text(data.slot_time);
            $('#booking-modal-sport').text(data.sport);
            $('#booking-modal-court').text(data.court);
            $('#booking-modal-username').text(data.name);
            $('#booking-modal-mobile').text(data.mobile);
            $('#booking-modal-email').text(data.email);
            $('#booked-modal-via').text(data.booked_via);
            $('#booked-modal-type').text(data.booked_type || '-');
            $('#booked-modal-price').text(data.slot_price);


            $('#booked-modal-discount').text(data.discount || 0);

            $('#booked-modal-amount').text(data.amount || 0);

            $('#booked-modal-credits').text(data.credits || 0);

            $('#booked-modal-coupon').text(data.coupon || '-');
            $('#booked-modal-payable-venue').text(data.payable_at_venue || 0);
            $('#booking-modal-bookedat').text(data.bookedat || '-');
            $('#booked-modal-bookedby').text(data.bookedby);
            $('#booking-modal-receipt').text(data.receipt || "No Receipt-ID");

            //  console.log(data.receipt)

            $('#booking-modal-booking').text(data.bookingid);
            $('#booking-modal-remarks').text(data.remarks || '-');

            if (data.status === 'success') {

                //      console.log(data.status);

                $('#calendar_booking_booking_info').show();
            }
        });
    };
    $('.bulkbooking_date').change(function () {
        // alert('hey');
        //    $('#user_info_calendarbooking_slots').hide();

        $('#ppuModal').modal('hide');


        $('.bulkbooking_daycheckbox').attr('checked', false);
        $('.bulkbooking_daycheckbox').attr("disabled", true);
        $("#bulkbooking_courts").select2("val", "");
        $("#bulkbooking_timings").select2("val", "");
        $('#submitbulkbookingforbooking').attr('disabled', 'disabled');
        $('#bulkbooking_timings').html('');
        var end_date_string = $('#end_date_bulkbooking').val();
        var start_date_string = $('#start_date_bulkbooking').val();
        if (start_date_string) {
            var start_date = new Date(start_date_string);
        }
        if (end_date_string) {
            var end_date = new Date(end_date_string);
        }
        if (end_date_string && start_date_string) {
            var len = 0;
            while (start_date.getTime() <= end_date.getTime() && len !== 7) {
                var day = start_date.getDay();
                if (day === 0) {
                    day = day + 6;
                } else {
                    day = day - 1;
                }
                $('input[value=' + day + '].bulkbooking_daycheckbox').attr("disabled", false);
                len = len + 1;
                start_date.setDate(start_date.getDate() + 1);
            }
        }
    });
    $('#bulkbooking_sport').change(function () {
        //    $('#user_info_calendarbooking_slots').hide();

        $('#ppuModal').modal('hide');


        $('#bulkbooking_courts').html('');
        var id = $('#bulkbooking_sport').val();
        $.get(document.URLS.courts(id), function (data, status) {
            $.each(data.courts, function (i, court) {
                $('#bulkbooking_courts').append($('<option>', {
                    value: court.id,
                    text: court.name
                }));
            });
        });
    });
    $('input.bulkbooking_daycheckbox').change(function () {
        timingSelect = [];
        courtSelect = [];
        timingSelect = $("#bulkbooking_timings").select2("val");
        courtSelect = $("#bulkbooking_courts").select2("val");
        //    $('#user_info_calendarbooking_slots').hide();


        $('#ppuModal').modal('hide');

        $('#bulkbooking_timings').html('');
        $('#bulkbooking_timings').append('<option disabled selected value> -- select an option -- </option>');
        $("#bulkbooking_courts").select2("val", "");
        $("#bulkbooking_timings").select2("val", "");
        $('#submitbulkbookingforbooking').attr('disabled', 'disabled');
        var checked = $('.bulkbooking_daycheckbox:checkbox:checked');
        var checked_arr = [];
        for (var i = 0; i < checked.length; i++) {
            checked_arr = checked_arr.concat(checked[i].value);
        }
        var bulkbooking_days_request = {
            days: checked_arr,
            sport: $('#bulkbooking_sport').val()
        };
        if (checked_arr.length > 0) {
            $.post(document.URLS.minworkinghours(), JSON.stringify(bulkbooking_days_request), function (data, status) {
                $.each(data.arr, function (i, timing) {
                    $('#bulkbooking_timings').append($('<option>', {
                        value: timing.start,
                        text: timing.start + ' - ' + timing.end
                    }));
                });
                if (timingSelect.length) {
                    $("#bulkbooking_timings").select2("val", timingSelect);
                    $("#bulkbooking_timings").trigger("change");
                }
                if (courtSelect.length)
                    $("#bulkbooking_courts").select2("val", courtSelect);
            });
        }
    });
    $('#bulkbooking_sport').change(function () {
        // $('#user_info_calendarbooking_slots').hide();

        $('#ppuModal').modal('hide');


        $('#bulkbooking_timings').html('');
        $('#bulkbooking_timings').append('<option disabled selected value> -- select an option -- </option>');
        $("#bulkbooking_courts").select2("val", "");
        $("#bulkbooking_timings").select2("val", "");
        $('#submitbulkbookingforbooking').attr('disabled', 'disabled');
        var checked = $('.bulkbooking_daycheckbox:checkbox:checked');
        var checked_arr = [];
        for (var i = 0; i < checked.length; i++) {
            checked_arr = checked_arr.concat(checked[i].value);
        }
        var bulkbooking_days_request = {
            days: checked_arr,
            sport: $('#bulkbooking_sport').val()
        };
        if (checked_arr.length > 0) {
            $.post(document.URLS.minworkinghours(), JSON.stringify(bulkbooking_days_request), function (data, status) {
                $.each(data.arr, function (i, timing) {
                    $('#bulkbooking_timings').append($('<option>', {
                        value: timing.start,
                        text: timing.start + ' - ' + timing.end
                    }));
                });
            });
        }
    });
    $('#calendar_sports').change(function () {
        document.getElementById("calendarbooking").submit();
    });
    $('#submitbulkbookingforbooking').click(function () {
        $('#premodal_unbooked').html('');
        var checkbox_arr_val = [];
        var checkbox_arr = $('.bulkbooking_daycheckbox:checkbox:checked');
        for (var i = 0; i < checkbox_arr.length; i++) {
            checkbox_arr_val = checkbox_arr_val.concat(checkbox_arr[i].value);
        }
        var data = {
            'bulkbooking_sport': $('#bulkbooking_sport').val(),
            'start_date_bulkbooking': $('#start_date_bulkbooking').val(),
            'end_date_bulkbooking': $('#end_date_bulkbooking').val(),
            'bulkbooking_timings': $('#bulkbooking_timings').val(),
            'bulkbooking_courts': $('#bulkbooking_courts').val(),
            'bulkbooking_day': checkbox_arr_val
        };
        $.post(document.URLS.bulkbookingcheck(), JSON.stringify(data), function (data, status) {

            var i = 0;
            var bodyHtmlStr = '';
            if (data.courts) {
                for (i = 0; i < data.unbooked_slots.length; i++) {
                    bodyHtmlStr = '' +
                        '<div class="col-md-4">Date: ' + data.unbooked_slots[i].date + '</div>' +
                        '<div class="col-md-4"> Slot From Time: ' + data.unbooked_slots[i].time + '</div>' +
                        '<div class="col-md-4"> Court: ' + data.unbooked_slots[i].court + '</div>' +
                        '<div class="col-md-12"></div>';
                    $('#premodal_unbooked').append(bodyHtmlStr);
                }
            } else {
                for (i = 0; i < data.unbooked_slots.length; i++) {
                    bodyHtmlStr = '' +
                        '<div class="col-md-12">Date: ' + data.unbooked_slots[i].date +
                        ' Slot From Time: ' + data.unbooked_slots[i].time + '</div>';
                    $('#premodal_unbooked').append(bodyHtmlStr);
                }
            }

            if (data.unbooked_slots.length === 0) {
                $('#premodal_unbooked').append('<div class="col-md-12">All Slots are Available!</div>');


                $('#ppuModal').appendTo("#userinfo_for_bulkbooking");


                //    $("#user_info_calendarbooking_slots").appendTo("#userinfo_for_bulkbooking");
                $("#calbooking_type_wrapper").hide();
                //    $('#user_info_calendarbooking_slots').show();

                $('#ppuModal').modal('show');


                if ($("#bulkbooking_type").val() != 3) {
                    $('#walkin_price').val(0);

                    $('#booking_amount').val(0);

                } else {
          
                    $('#walkin_price').val(data.cost);

                    $('#booking_amount').val(data.cost);

                }
            //    $("#walkin_price").prop("readonly", false);

                $('#booking_amount').prop("readonly", false);

            }
            $('#prebulkbooking_modal').show();
        });
    });
    $('#downloadcsvform').click(function () {
        var start = $('#start_date_downloadcsv').val().trim();
        var end = $('#end_date_downloadcsv').val().trim();
        var report_type = $('#report_type').val();
        var venue_id = $("#sportscenter_id").val();
        $('#start_date_downloadcsv').val(" ");
        $('#end_date_downloadcsv').val(" ");
        if (!start) {
            swal("Oops", "Please enter start date..!", "error")

            //            alert("Please enter start date ");
            return false;
        } else {
            start = new Date(start);
        }
        if (!end) {

            swal("Oops", "Please enter end date..!", "error")


            //    alert("Please enter end date ");
            return false;
        } else {
            end = new Date(end);
        }
        if (end < start) {

            swal({
                title: "Oops.!",
                text: "End date should be greater than start date..!",
                icon: "info",
            })


            return false;
        }

        if (start && end) {
            var start_month = start.getMonth() + 1;
            var end_month = end.getMonth() + 1;
            var url = document.URLS.generate_csv() + '?start_year=' + start.getFullYear() + '&start_month=' + start_month + '&start_day=' + start.getDate() + '&end_year=' + end.getFullYear() + '&end_month=' + end_month + '&end_day=' + end.getDate() + '&type=' + report_type + '&venue_id=' + venue_id;
            location.href = url;
        }
        return false;
    });

   


    $('#submitslotsforbooking').click(function () {
        $('#ppuModal').modal('show');


        //    $('#user_info_calendarbooking_slots').show();

      //  $("#walkin_price").prop('disabled', false);
          

    
        $("#booking_amount").prop("readonly", false);


        $('#submitslotsforbooking').hide();
    });
    $('#user_email').blur(function () {
        var data = {
            'email': $('#user_email').val(),
        };
        $.post(document.URLS.verify_email(), JSON.stringify(data), function (data, status) {
            if (data.status == 'success') {
                var update_mobile_string = "Email already exists with mobile number " + data.mobile + ".Do you wish to use the old number? ";
                var update_mobile = confirm(update_mobile_string);
                if (update_mobile == true) {
                    $('#user_mobile').val(data.mobile);
                }
                $('#walkin_user_name').val(data.name);
            }
        });
    });
    $('#user_mobile').blur(function () {
        var data = {
            'mobile': $('#user_mobile').val(),
        };
        $.post(document.URLS.verify_mobile(), JSON.stringify(data), function (data, status) {
            $('#useremail_formgroup').show();
            $('#username_formgroup').show();
            if (data.status == 'success') {
                if (data.email.slice(-11) !== 'invalid.com') {
                    $('#user_email').val(data.email);
                    $("#user_email").prop('disabled', true);
                } else {
                    $('#user_email').val('');
                    $("#user_email").prop('disabled', false);
                }
                $('#walkin_user_name').val(data.name);
                $("#walkin_user_name").prop('disabled', true);
            } else {
                $('#walkin_user_name').val('');
                $('#user_email').val('')
                $("#walkin_user_name").prop('disabled', false);
                $("#user_email").prop('disabled', false);
            }
        });
    });
    var calendar_add_slots = (function () {
        var selected_slots = {};
        var slots_list = [];
        return {
            add_slots_to_receipt: function (slotid, timing, courtname, price) {
                PRICE = Number($('#walkin_price').val()) + Number(price);

                $('#booking_amount').val(PRICE)
                $('#booking_discount').val(0);

                if (!selected_slots[slotid]) {
                    if (CURRENT_SELECTED_OPTION != 3) {
                        $('#walkin_price').val(0);
                        $('#booking_amount').val(0)
                        $('#booking_discount').val(0);

                    } else {
                        $('#walkin_price').val(Number($('#walkin_price').val()) + Number(price));

                        //   $('#booking_amount').val(Number($('#walkin_price').val()) + Number(price));
                        //  $('#booking_amount').val(PRICE)    
                        $('#booking_discount').val(0);


                    }

                    selected_slots[slotid] = {
                        'court': courtname,
                        'timing': timing
                    };
                    $("#" + slotid).css("background-color", "orange");
                    var divslotid = 'selectedcourt' + slotid;

                    var divslotid_modal = 'selectedcourt_modal' + slotid;
                    var hiddenlotid = 'hiddenslot' + slotid;
                    slots_list.push(slotid);


                    var selected_court_div = '<div class="selectedcourt badge badge-pill badge-info"  style="margin-left:5px;margin-bottom:5px;font-size:15px;background-color: #33b597" id="' + divslotid + '"> Time: ' + timing + '  Court: ' + courtname + ' </div>';

                    var selected_court_div_in_modal = '<div class="selectedcourt badge badge-pill badge-info"  style="margin-left:5px;margin-bottom:5px;font-size:15px;background-color: #33b597" id="' + divslotid_modal + '"> Time: ' + timing + '  Court: ' + courtname + ' </div>';

                    var selected_slot_hidden_input = '<input type="hidden" name="hiddenslotid" id="' + hiddenlotid + '" value="' + slotid + '">';


                    $('#selectedcourts').append(selected_court_div);

                    $('.Selected_Courts').append(selected_court_div_in_modal)

                    // $('.selectedcourt').append(selected_court_div)


                    $('#hiddenslotinputs').append(selected_slot_hidden_input);
                } else if (selected_slots[slotid]) {
                    PRICE = Number($('#walkin_price').val()) - Number(price)
                    $('#walkin_price').val(Number($('#walkin_price').val()) - Number(price));

                    //   $('#booking_amount').val(Number($('#walkin_price').val()) - Number(price));


                    $('#booking_amount').val(PRICE)

                    $("#" + slotid).css("background-color", "#099a8c");
                    delete selected_slots[slotid];
                    var divslotid = 'selectedcourt' + slotid;

                    var divslotid_modal = 'selectedcourt_modal' + slotid;

                    var hiddenlotid = 'hiddenslot' + slotid;

                    $('#' + divslotid).remove();

                    $('#' + divslotid_modal).remove();


                    //    $('.Selected_Courts').remove()

                    $('#' + hiddenlotid).remove();
                }

                if (Object.keys(selected_slots).length === 0) {
                    //    $('#user_info_calendarbooking_slots').hide();

                    $('#ppuModal').modal('hide');

                    $('#submitslotsforbooking').hide();
                    $('#cancelslotsforbooking').hide();
                } else {
                    $('#submitslotsforbooking').show();
                    $('#cancelslotsforbooking').show();
                }
            },
            remove_slots: function () {
                var slot;
                var len = slots_list.length;
                var i = 0;
                while (i < len) {
                    $("#" + slots_list[i]).css("background-color", "#099a8c");
                    i++;
                }
                slots_list = [];
                selected_slots = {};
                $('#selectedcourts').html('');

                $('.Selected_Courts').html('')

                $('#hiddenslotinputs').html('');
                // $('#user_info_calendarbooking_slots').hide();

                $('#ppuModal').modal('hide');

                $('#user_mobile').val('');

                $('#useremail_formgroup').hide();
                $('#username_formgroup').hide();
                PRICE = 0;

                $('#walkin_price').val(0);

                $('#booking_amount').val(0)

                $('#booking_discount').val(0);

                $('#payment_method').val("NULL");
                $('#walkin_remarks').val('');
            },
        };
    })();
    window.ppu.add_slots_to_receipt = calendar_add_slots.add_slots_to_receipt;
    window.ppu.cancel_slots = calendar_add_slots.remove_slots;
    var disable_submit_button = (function () {
        return {
            fun: function () {
                //    $('#user_info_calendarbooking_slots').hide();


                $('#ppuModal').modal('hide');



                if ($('#bulkbooking_timings').val()) {
                    $('#submitbulkbookingforbooking').removeAttr('disabled');
                } else {
                    $('#submitbulkbookingforbooking').attr('disabled', 'disabled');
                }
            }
        };
    })();

    $('#bulkbooking_timings').change(disable_submit_button.fun);
    $('#bulkbooking_courts_div').change(function () {

        // $('#user_info_calendarbooking_slots').hide();

        $('#ppuModal').modal('hide');

    });
    disable_submit_button.fun();
    //Singleton instance for returning Template for GET for bookings.
    var template_instance = (function () {
        past_booking_row = '<tr id="{0}">\
                                <td class="shrink"> {0} </td>\
                                <td class="shrink"><i onclick="ppu.downloadreceipt({2})" class="fa fa-cloud-download"></i></td>\
                                <td class="shrink">{1}</td>\
                                <td class="shrink">{3} {4}\
                                </td>\
                                <td class="shrink">{5}</td>\
                                <td class="shrink">{6}</td>\
                                <td class="shrink">{7}</td>\
                                <td class="shrink">{8}</td>\
                                <td class="shrink">{9}\
                                </td>\
                                <td class="shrink">{10}</td>\
                                <td class="shrink">{11}</td>\
                                <td class="shrink">{12}</td>\
                             </tr>\
        '

        current_booking_row = '<tr id="{0}">\
                                <td class="shrink">{0}</td>\
                                <td class="shrink"><i onclick="ppu.downloadreceipt({2})" class="fa fa-cloud-download"></i></td>\
                                <td class="shrink"><i style="color: #1f1f1f; cursor: pointer; cursor: hand;" class="fa fa-times" id="cancelbooking_booking_{2}" onclick="ppu.add_cancelbooking({2},{0})"></i></td>\
                                <td class="shrink">{1}</td>\
                                <td class="shrink">{3} {4}</td>\
                                <td class="shrink">{5}</td>\
                                <td class="shrink">{6}</td>\
                                <td class="shrink">{7}</td>\
                                <td class="shrink">{8}</td>\
                                <td class="shrink">{9}</td>\
                                <td class="shrink">{10}</td>\
                                <td class="shrink">{11}</td>\
                                <td class="shrink">{12}</td>\
                             </tr>\
        '

        cancelled_booking_row = '<tr id="{0}">\
                               <td class="shrink">{0}</td>\
                                <td class="shrink"><i onclick="ppu.downloadreceipt({2})" class="fa fa-cloud-download"></i></td>\
                                <td class="shrink">{1}</td>\
                                <td class="shrink">{3} {4}\
                                </td>\
                                <td class="shrink">{5}</td>\
                                <td class="shrink">{6}</td>\
                                <td class="shrink">{7}</td>\
                                <td class="shrink">{8}</td>\
                                <td class="shrink">{9}\
                                </td>\
                                <td class="shrink">{10}</td>\
                                <td class="shrink">{11}</td>\
                                <td class="shrink">{12}</td>\
                                <td class="shrink">{13}</td>\
                                <td class="shrink">{14}</td>\
                             </tr>\
        '

        return {
            past_booking: function (arr) {
                return past_booking_row.format(arr)
            },
            current_booking: function (arr) {
                return current_booking_row.format(arr)
            },
            cancelled_booking_row: function (arr) {
                return cancelled_booking_row.format(arr)
            }
        }
    })();

    //Singleton instance for returning utils.
    var utils_ppu = (function () {

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        var add_one_minute_to_string = function (naive) {

            return new Date(new Date(naive).getTime() + 1000 * 60);
        }
        return {
            court_name: function (arr) {
                name = ''
                for (i = 0; i < arr.length; i++) {
                    name = name.concat(arr[i].name)
                }
                return name
            },
            datefromDate: function (date) {
                return monthNames[date.getUTCMonth()] + ' ' + date.getUTCDate() + ',' + date.getUTCFullYear()
            },
            timefromslot: function (slot) {
                slot_from = new Date(slot.slot_from)
                slot_to = add_one_minute_to_string(slot.slot_to)
                slot_timings = slot_from.getUTCHours() + ':' + (slot_from.getUTCMinutes() < 10 ? '0' : '') + slot_from.getUTCMinutes() + ' - ' + slot_to.getUTCHours() + ':' + (slot_to.getUTCMinutes() < 10 ? '0' : '') + slot_to.getUTCMinutes()

                return slot_timings
            }
        }
    })();
    //Singleton instance for fetching bookings.
    var populate_bookings = (function () {
        return {
            past: function (url) {
                $.get(url, function (data, status) {
                    bookings = data.results;
                    booking = bookings[0]
                    for (var i = 0; i < bookings.length; i++) {
                        booking = bookings[i]
                        if (booking.transaction) {
                            if (!booking.amount && booking.amount !== 0) {
                                amount = booking.transaction.amount
                            } else {
                                amount = booking.amount
                            }
                        } else {
                            amount = 'na'
                        }
                        if (booking.bulk_booking != null) {
                            bulk_booking_id = booking.bulk_booking
                        } else {
                            bulk_booking_id = ''
                        }
                        if (booking.remarks != null) {
                            remarks = booking.remarks
                        } else {
                            remarks = ''
                        }
                        arr = [
                            bulk_booking_id,
                            booking.bookingID,
                            booking.id,
                            booking.user.first_name,
                            booking.user.last_name,
                            booking.user.mobile,
                            booking.sport.sport.name,
                            utils_ppu.court_name(booking.court.space_name),
                            utils_ppu.datefromDate(new Date(booking.slot.slot_from)),
                            utils_ppu.timefromslot(booking.slot),
                            amount,
                            utils_ppu.datefromDate(new Date(booking.created_at)),
                            remarks
                        ]
                        $('#pastbookingtable_body').append(template_instance.past_booking(arr));
                    }

                    if (data.next) {
                        populate_bookings.past(data.next);
                    } else {
                        if ($('#pastbookingtable').length) {
                            $('#pastbookingtable').DataTable({
                                "aoColumnDefs": [{
                                    'bSortable': false,
                                    'aTargets': [0, 9]
                                }]
                            });
                        }
                    }

                });
            },
            current: function (url) {
                $.get(url, function (data, status) {
                    bookings = data.results;
                    console.log(bookings);
                    for (var i = 0; i < bookings.length; i++) {
                        booking = bookings[i]
                        if (booking.transaction) {
                            if (!booking.amount && booking.amount !== 0) {
                                amount = booking.transaction.amount
                            } else {
                                amount = booking.amount
                            }
                        } else {
                            amount = 'na'
                        }
                        if (booking.bulk_booking != null) {
                            bulk_booking_id = booking.bulk_booking
                        } else {
                            bulk_booking_id = ''
                        }
                        if (booking.remarks != null) {
                            remarks = booking.remarks
                        } else {
                            remarks = ''
                        }

                        arr = [
                            bulk_booking_id,
                            booking.bookingID,
                            booking.id,
                            booking.user.first_name,
                            booking.user.last_name,
                            booking.user.mobile,
                            booking.sport.sport.name,
                            utils_ppu.court_name(booking.court.space_name),
                            utils_ppu.datefromDate(new Date(booking.slot.slot_from)),
                            utils_ppu.timefromslot(booking.slot),
                            amount,
                            utils_ppu.datefromDate(new Date(booking.created_at)),
                            remarks,
                        ]

                        $('#currentbookingtable_body').append(template_instance.current_booking(arr));
                    }

                    if (data.next) {
                        populate_bookings.current(data.next);
                    } else {
                        if ($('#currentbookingtable').length)
                            $('#currentbookingtable').DataTable();
                    }

                });

            },
            cancelled: function (url) {
                $.get(url, function (data, status) {
                    bookings = data.results;
                    for (var i = 0; i < bookings.length; i++) {
                        booking = bookings[i]
                        if (booking.booking.transaction) {
                            if (!booking.booking.amount && booking.booking.amount !== 0) {
                                amount = booking.booking.transaction.amount
                            } else {
                                amount = booking.booking.amount
                            }
                        } else {
                            amount = 'na'
                        }
                        if (booking.booking.bulk_booking != null) {
                            bulk_booking_id = booking.booking.bulk_booking
                        } else {
                            bulk_booking_id = ''
                        }
                        if (booking.booking.remarks != null) {
                            bulk_booking_remark = booking.booking.remark
                        } else {
                            bulk_booking_remark = ''
                        }

                        arr = [bulk_booking_id,
                            booking.booking.bookingID,
                            booking.booking.id,
                            booking.booking.user.first_name,
                            booking.booking.user.last_name,
                            booking.booking.user.mobile,
                            booking.booking.sport.sport.name,
                            utils_ppu.court_name(booking.booking.court.space_name),
                            utils_ppu.datefromDate(new Date(booking.booking.slot.slot_from)),
                            utils_ppu.timefromslot(booking.booking.slot),
                            amount,
                            utils_ppu.datefromDate(new Date(booking.booking.created_at)),
                            utils_ppu.datefromDate(new Date(booking.created_at)),
                            booking.booking.remarks,
                            booking.remark,
                        ]

                        $('#cancelledbookingtablebody').append(template_instance.cancelled_booking_row(arr));
                    }


                    if (data.next) {
                        populate_bookings.cancelled(data.next)
                    } else {
                        if ($('#cancelledbookingtable').length) {
                            $('#cancelledbookingtable').DataTable({
                                "aoColumnDefs": [{
                                    'bSortable': false,
                                    'aTargets': [0, 9]
                                }]
                            });
                        }
                    }

                });

            }
        }

    })();

    populate_bookings.past(document.URLS.past_bookings(document.KEYS.getcookie('sportscenter_key')));
    populate_bookings.current(document.URLS.current_bookings(document.KEYS.getcookie('sportscenter_key')));
    populate_bookings.cancelled(document.URLS.cancelled_bookings(document.KEYS.getcookie('sportscenter_key')));

});