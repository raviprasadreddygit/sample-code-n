

swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    swal("Poof! Your imaginary file has been deleted!", {
      icon: "success",
    });
  } else {
    swal("Your imaginary file is safe!");
  }
});



function postEndEnrollmentDetails() {
    $('#errEnrollment').hide();
    $("#edit_enrollement_confirm_id").attr("disabled", false);
    var subscriptionId = currentEnroll.subscription[currentEnrollIndex].id;
    var pendingAmount = $('#txtPendingAmount').val();
    var endDate = $('#txtEndDate').val();
    var data = {
        'subscriptionId': subscriptionId,
        'pendingAmount': pendingAmount,
        'endDate': endDate
    }
    $.ajax({
        url: SERVER + 'coaching/end_enrollment/',
        data: data,
        type: 'POST',
        success: function (result) {
            try {
                if (result.success === "true") {

                    swal("successful");


                    $('#edit_enrollement_confirm_id').attr("disabled", true);
                    $('#endEnrollModal').modal('hide');
                } else {
                    $('#edit_enrollement_confirm_id').attr("disabled", false);
                    $('#endEnrollModal').modal('show');
                    $('#errEnrollment').show();
                }
            } catch (error) {
                $('#endEnrollModal').modal('show');
            }
            $('#viewEnrollmentModal').modal('hide');
            $('#edit_enrollement_confirm_id').attr("disabled", false);
            //        searchEnrollments();
        },
        error: function (e) {

            swal("Oops", "something went wrong please try again..!", "error")
        }
    });
}



-----------------------------------------------


    <form method="post" id="calendarbooking">
                                                    <input type="hidden" name="form_type" value="calendarbooking">

                                                    <input type="hidden" name="calendar_sports"  id="calendar_sports"   value="calendarbookingvalues" class="calendar_sports" >

                                                    <div class="form-group col-md-4">
                                                        <label class="form-label">Sport</label>
                                                        <div class="controls">
                                                            <!-- <select name="calendar_sports" id="calendar_sports"  class="calendar_sports"  multiple="multiple"> -->
                                                       
                                                            <select name="calendar_sports1" id="calendar_sports1"  class="calendar_sports1"  multiple="multiple">
                                                                    <!--<option disabled selected value> &#45;&#45; select an option &#45;&#45;</option>-->
                                                                {% for sport in sports %}
                                                                {% if sport == cal_selected_sport %}
                                                                <option value="{{ sport.id }}"  selected="selected" >{{ sport.sport.name}} </option>
                                                                {% else %}
                                                                <option value="{{ sport.id }}">{{ sport.sport.name }} </option>
                                                                {% endif %}
                                                                {% endfor %}
                                                                {% if is_all %}
                                                                <option value="ALL"  >All</option>
                                                                {% else %}
                                                                <option value="ALL"    >All</option>
                                                                {% endif %}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="form-label">Date</label>
                                                        <div class="controls">

                                                            <input id="calendar_date" name="calendar_date" data-date-format="mm/dd/yyyy"
                                                                data-language="en" autocomplete="off" class="form-control datepicker-here"
                                                                value="{{ today | date:'m/d/Y' }}">


                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label cl
                                                        
                                                        ass="form-label">&nbsp;</label>
                                                        <div class="controls">
                                                            <input type="submit" name="submit" value="Get Slots"  id="getSportName_Id"  onclick="ppu.getSportName()" class="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </form>
                                            






                                              $('#calendar_sports1').multipleSelect({
        selectAll: false,
        filter: true
    });
 


    // $('#calendar_sports').change(function(){
    //     var meals = $(this).val();
    //     var selectedmeals = meals.join(", "); // there is a break after comma
    
    //     alert (selectedmeals); // just for testing what will be printed
    // })



    
    

    window.ppu.getSportName = function getSportName() {

      //   var selectedValues = $('#calendar_sports1').val();
       //  alert(selectedValues)

        var values = Array.prototype.slice.call(document.querySelectorAll('#calendar_sports1 option:checked'),0).map(function(v,i,a) {
            return v.value;
        });


      
        alert(values);

        $('#calendar_sports').val($('#calendar_sports1').val())
  


    //    var x = $('input[name="calendar_sports"]').val();


      //  alert(x);

        // var selMulti = $.map($("#calendar_sports option:selected"), function (el, i) {
        //     return $(el).val();
        // });
      
        // alert(selMulti.join(", "))

        // var select_val = [] ;
        // $("#calendar_sports :selected").each(function (i,sel) {
        //     select_val.push($(sel).val());
        //   });

        //  alert(select_val);
           
            
        
  //      var selectval =   $("#calendar_sports option").filter(":selected").val();

//        alert(selectval);


    //     var selected=[];
    //     $('#calendar_sports :selected').each(function(){
    //       selected[$(this).val()]=$(this).val();
    //     });
    //    alert(selected);
   

    }


  -------------------------------



     window.baseedit.getData = function() {
    //    var obj = document.getElementById("multi").val();
        var values = Array.prototype.slice.call(document.querySelectorAll('#amenities option:checked'),0).map(function(v,i,a) {
        return v.value;
    });

    var data  = {
        'Amenities': values
    }
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", document.URLS.update_amenities(), true);
    xhttp.send(JSON.stringify(data));
    }

    $.get(document.URLS.update_amenities(), function(data, status){
        if(data.error){
            $("#amenities").select2();
        }
        else{
            $("#amenities").val(data).select2();
        }


    });
